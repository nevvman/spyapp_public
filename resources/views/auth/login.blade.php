@extends('layouts.app')

@section('content')
<div class="portlet light">
	<div class="portlet-title">
		<div class="caption  font-green-sharp">
			<i class="fa fa-sign-in font-green" aria-hidden="true"></i>
			<span class="caption-subject bold uppercase"> Login</span>

		</div>

	</div>
	<div class="portlet-body">
		<form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
			{{ csrf_field() }}

			<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
				<label for="email" class="col-md-4 control-label">E-Mail Address</label>

				<div class="col-md-6 input-group">
					<span class="input-group-addon input-circle-left">
						<i class="fa fa-envelope font-green"></i>
					</span>
					<input id="email" type="email" class="form-control input-circle-right" name="email" value="{{ old('email') }}" required autofocus>

				</div>

			</div>

			<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
				<label for="password" class="col-md-4 control-label">Password</label>

				<div class="col-md-6 input-group">
					<span class="input-group-addon input-circle-left">
						<i class="fa fa-unlock-alt font-green" style="font-size: 20px;"></i>
					</span>
					<input id="password" type="password" class="form-control input-circle-right" name="password" required>

				</div>

			</div>

			<div class="form-group">
				<div class="col-md-6 col-md-offset-4">
					<div class="mt-checkbox mt-checkbox-outline font-green">
						<label >
							<input type="checkbox" name="remember" {{ old('remember') ? 'checked' : ''}}> Remember Me
						</label>
					</div>
				</div>
			</div>
			@if ($errors->has('email'))
			<span class="help-block note note-success" style="margin-left: 35%;">
				<strong>{{ $errors->first('email') }}</strong>
			</span>
			<br>
			@endif
			@if ($errors->has('password'))
			<span class="help-block note note-success"style="margin-left: 35%;">
				<strong>{{ $errors->first('password') }}</strong>
			</span>
			<br>
			@endif

			<div class="form-group">
				<div class="col-md-8 col-md-offset-4" style="">
					<button type="submit" class="btn green-haze btn-outline sbold uppercase">
						Login
					</button>

					<a class="btn blue btn-outline sbold uppercase" href="redirect"> <i class="fa fa-facebook-square" aria-hidden="true" style="font-size: 20px;"></i> Login</a>
					or
					<span>
						<a class="btn green-haze btn-outline sbold uppercase" href="{{ url('/password/reset') }}">
							Forgot Your Password?
						</a>
					</span>
				</div>
			</div>
		</form>
	</div>
</div>
@endsection
