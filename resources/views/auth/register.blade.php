@extends('layouts.app')

@section('content')
<div class="portlet light">
	<div class="portlet-title">
		<div class="caption  font-green-sharp">
			<i class="fa fa-registered font-green" aria-hidden="true"></i>
			<span class="caption-subject bold uppercase"> Register</span>

		</div>

	</div>
	<div class="portlet-body">
		<form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
			{{ csrf_field() }}

			<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
				<label for="name" class="col-md-4 control-label">Name</label>

				<div class="col-md-6 input-group">
					<span class="input-group-addon input-circle-left">
						<i class="fa font-green fa-user" aria-hidden="true"></i>
					</span>
					<input id="name" type="text" class="form-control input-circle-right" name="name" value="{{ old('name') }}" required autofocus>

				</div>
			</div>

			<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
				<label for="email" class="col-md-4 control-label">E-Mail Address</label>

				<div class="col-md-6 input-group">
					<span class="input-group-addon input-circle-left">
						<i class="fa font-green fa-envelope"></i>
					</span>
					<input id="email" type="email" class="form-control input-circle-right" name="email" value="{{ old('email') }}" required>

				</div>
			</div>

			<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
				<label for="password" class="col-md-4 control-label">Password</label>

				<div class="col-md-6 input-group">
					<span class="input-group-addon input-circle-left">
						<i class="fa fa-unlock-alt font-green" style="font-size: 20px;"></i>
					</span>
					<input id="password" type="password" class="form-control input-circle-right" name="password" required>

				</div>
			</div>

			<div class="form-group">
				<label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

				<div class="col-md-6 input-group">
					<span class="input-group-addon input-circle-left">
						<i class="fa fa-unlock-alt font-green" style="font-size: 20px;"></i>
					</span>
					<input id="password-confirm" type="password" class="form-control input-circle-right" name="password_confirmation" required>
				</div>
			</div>

					@if ($errors->has('name'))
					<span class="help-block note note-success" style="margin-left: 35%;" >
						<strong>{{ $errors->first('name') }}</strong>
					</span>
					<br>
					@endif
					@if ($errors->has('email'))
					<span class="help-block note note-success" style="margin-left: 35%;">
						<strong>{{ $errors->first('email') }}</strong>
					</span>
					<br>
					@endif
					@if ($errors->has('password'))
					<span class="help-block note note-success" style="margin-left: 35%;">
						<strong>{{ $errors->first('password') }}</strong>
					</span>
					<br>
					@endif
			<div class="form-group">
				<div class="col-md-6 col-md-offset-4">
					<button type="submit" class="btn green-haze btn-outline sbold uppercase">
						Register
					</button>
					<a class="btn blue btn-outline sbold uppercase" href="redirect"> <i class="fa fa-facebook-square" aria-hidden="true" style="font-size: 20px;"></i> Register</a>
				</div>
			</div>
		</form>
	</div>
</div>
</div>
</div>
</div>
@endsection
