
@extends('layouts.app')

@section('content') 
<!--<img src="{!! $link !!}" class="img-responsive img-circle" alt=""> </div>-->
<div class="col-md-6" style="margin-left: 25%;">
	<div class="portlet light bordered">
		<div class="portlet-title">
			<div class="caption font-green-sharp">
				<i class="icon-speech font-green-sharp"></i>
				<span class="caption-subject bold uppercase"> Personal information</span>
			</div>
		</div>
		<div class="portlet-body">
			<span class="font-green bold uppercase" style="font-size: 17px;padding: 45%;">{{ Auth::user()->name }}</span>
			<div class="row" style="margin-top: 20px;">
				<a href="#avatars"><div id="avatars" class="col-md-4">
					<img src="{!! $link !!}" id="avatar" class="img-responsive" alt="">     <br>
					<form id="file" enctype="multipart/form-data"  class="form-horizontal" role="form" method="POST" action="{{ url('/user/edit') }}">
						{{ csrf_field() }}
						<label class="btn green-haze btn-outline sbold uppercase btn-file" style="margin-left: 20%">
							Add new avatar <input type="file" accept="image/jpeg" name="img" style="display: none;">
						</label>
					</form>
					</div></a>


				<div class="col-md-8">
					<form class="form-horizontal" role="form" method="POST" action="{{ url('/user/edit') }}">
						{{ csrf_field() }}
						<div class="form-group">
							<label for="first_name" class="col-md-4 control-label">First name</label>
							<div class="col-md-6 input-group">
								<span class="input-group-addon input-circle-left">
									<i class="fa fa-user font-green" aria-hidden="true" style="font-size: 20px;"></i>
								</span>
								<input id="first_name" type="text" class="form-control  input-circle-right" name="first_name" value="{{ Auth::user()->first_name }}" autofocus>

							</div>

						</div>
						<div class="form-group">
							<label for="last_name" class="col-md-4 control-label">Last name</label>

							<div class="col-md-6 input-group">
								<span class="input-group-addon input-circle-left">
									<i class="fa fa-user font-green" aria-hidden="true" style="font-size: 20px;"></i>
								</span>
								<input id="last_name" type="text" class="form-control  input-circle-right" name="last_name" value="{{ Auth::user()->last_name }}" autofocus>
							</div>

						</div>
						<div class="form-group">
							<label for="middle_name" class="col-md-4 control-label">Middle name</label>

							<div class="col-md-6 input-group">
								<span class="input-group-addon input-circle-left">
									<i class="fa fa-user font-green" aria-hidden="true" style="font-size: 20px;"></i>
								</span>
								<input id="middle_name" type="text" class="form-control  input-circle-right" name="middle_name" value="{{ Auth::user()->middle_name }}" autofocus>
							</div>

						</div>
						<div class="form-group">
							<label for="name" class="col-md-4 control-label">Name(login)</label>

							<div class="col-md-6 input-group">
								<span class="input-group-addon input-circle-left">
									<i class="fa fa-user font-green" aria-hidden="true" style="font-size: 20px;"></i>
								</span>
								<input id="name" type="text" class="form-control  input-circle-right" name="name" value="{{ Auth::user()->name }}" autofocus>
							</div>

						</div>
						<div class="form-group">
							<label for="email" class="col-md-4 control-label">E-Mail Address</label>

							<div class="col-md-6 input-group">
								<span class="input-group-addon input-circle-left">
									<i class="fa fa-envelope font-green"></i>
								</span>
								<input id="email" type="email" class="form-control input-circle-right" name="email" value="{{ Auth::user()->email }}" required autofocus>

							</div>

						</div>
						@if(Session::has('msg'))
						<span id="msg" class="help-block note note-success"style="margin-left: 35%;">
							<strong>{{Session::get('msg')}}</strong>
						</span>
						<script>$(function () {
								if ($("#msg").text() == "Avatar updated") {
									setInterval(function () {
										$("#avatars").load("http://spyapp.dew/user#avatars");
									}, 1000);
								}
							});</script>
						@endif
						<div class="form-group">
							<div class="col-md-8 col-md-offset-4" style="">
								<button type="submit" class="btn green-haze btn-outline sbold uppercase" style="float: right; margin-right: 20%;">
									Save
								</button>
								<button type="button" class="btn green-haze btn-outline sbold uppercase" data-toggle="modal" data-target="#myModal">Edit password</button>
							</div>
						</div>

					</form>
				</div>
			</div>
			<div id="myModal" class="modal fade" role="dialog">
				<div class="modal-dialog">

					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">Edit password</h4>
						</div>
						<div class="modal-body">
							<form class="form-horizontal" role="form" method="POST" action="{{ url('/user/edit') }}">
								{{ csrf_field() }}
								<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
									<label for="password" class="col-md-4 control-label">Old Password</label>

									<div class="col-md-6 input-group">
										<span class="input-group-addon input-circle-left">
											<i class="fa fa-unlock-alt font-green" style="font-size: 20px;"></i>
										</span>
										<input id="password" type="password" class="form-control input-circle-right" name="old_password" required>

									</div>
								</div>

								<div class="form-group">
									<label for="password-confirm" class="col-md-4 control-label">New Password</label>

									<div class="col-md-6 input-group">
										<span class="input-group-addon input-circle-left">
											<i class="fa fa-unlock-alt font-green" style="font-size: 20px;"></i>
										</span>
										<input id="password-confirm" type="password" class="form-control input-circle-right" name="new_password" required>
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-6 col-md-offset-4">
										<button type="submit" class="btn green-haze btn-outline sbold uppercase" style="float:right;">
											save password
										</button>
									</div>
								</div>
							</form>


						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						</div>
					</div>

				</div>
			</div>

		</div>
	</div>
</div> 
<script>
	$(function () {
		$('form input[type=file]').on('change', function () {
			$("#file").submit();
			/*		var fd = new FormData;
			 fd.append('img', $(this).prop('files')[0]);
			 $.ajaxPrefilter(function (options) {
			 if (!options.beforeSend) {
			 options.beforeSend = function (xhr) {
			 xhr.setRequestHeader('X-CSRF-TOKEN', $('#file meta[name="_token"]').attr('content'));
			 });
			 }
			 });
			 $.ajax({
			 url: '/user/edit',
			 data: fd,
			 processData: false,
			 contentType: false,
			 type: 'POST',
			 success: function (data) {
			 alert(data);
			 }
			 });*/
		});
	});

</script>
<script src="../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="../assets/global/plugins/dropzone/dropzone.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="../assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="../assets/pages/scripts/form-dropzone.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="../assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
<script src="../assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
<script src="../assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
<script src="../assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>
@endsection