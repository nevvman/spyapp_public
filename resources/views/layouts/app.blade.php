<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.7
Version: 4.7.1
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>Spyapp</title>
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<meta content="width=device-width, initial-scale=1" name="viewport" />
			<meta content="Preview page of Metronic Admin Theme #1 for statistics, charts, recent events and reports" name="description" />
			<meta content="" name="author" />
			<!-- BEGIN GLOBAL MANDATORY STYLES -->
			<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
			<link href="/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
			<link href="/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
			<link href="/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
			<link href="/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
			<!-- END GLOBAL MANDATORY STYLES -->
			<!-- BEGIN PAGE LEVEL PLUGINS -->
			<link href="/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
			<link href="/assets/global/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
			<link href="/assets/global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css" />
			<link href="/assets/global/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css" />
			<!-- END PAGE LEVEL PLUGINS -->
			<!-- BEGIN THEME GLOBAL STYLES -->
			<link href="/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
			<link href="/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
			<!-- END THEME GLOBAL STYLES -->
			<!-- BEGIN THEME LAYOUT STYLES -->
			<link href="/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
			<link href="/assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
			<link href="/assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
			<!-- END THEME LAYOUT STYLES -->
			<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js"></script>
			<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
			<link rel="apple-touch-icon" sizes="57x57" href="/assets/favicon/apple-icon-57x57.png">
			<link rel="apple-touch-icon" sizes="60x60" href="/assets/favicon/apple-icon-60x60.png">
			<link rel="apple-touch-icon" sizes="72x72" href="/assets/favicon/apple-icon-72x72.png">
			<link rel="apple-touch-icon" sizes="76x76" href="/assets/favicon/apple-icon-76x76.png">
			<link rel="apple-touch-icon" sizes="114x114" href="/assets/favicon/apple-icon-114x114.png">
			<link rel="apple-touch-icon" sizes="120x120" href="/assets/favicon/apple-icon-120x120.png">
			<link rel="apple-touch-icon" sizes="144x144" href="/assets/favicon/apple-icon-144x144.png">
			<link rel="apple-touch-icon" sizes="152x152" href="/assets/favicon/apple-icon-152x152.png">
			<link rel="apple-touch-icon" sizes="180x180" href="/assets/favicon/apple-icon-180x180.png">
			<link rel="icon" type="image/png" sizes="192x192"  href="/assets/favicon/android-icon-192x192.png">
			<link rel="icon" type="image/png" sizes="32x32" href="/assets/favicon/favicon-32x32.png">
			<link rel="icon" type="image/png" sizes="96x96" href="/assets/favicon/favicon-96x96.png">
			<link rel="icon" type="image/png" sizes="16x16" href="/assets/favicon/favicon-16x16.png">
			<link rel="manifest" href="/assets/favicon/manifest.json">
			<meta name="msapplication-TileColor" content="#ffffff">
			<meta name="msapplication-TileImage" content="/assets/favicon/ms-icon-144x144.png">
			<meta name="theme-color" content="#ffffff">
		<!-- END HEAD -->

		<body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-content-white">
			<script   src="https://code.jquery.com/jquery-2.2.4.min.js"   integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="   crossorigin="anonymous"></script>
			<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
			<div class="page-header navbar navbar-fixed-top">
				<!-- BEGIN HEADER INNER -->
				<div class="page-header-inner ">
					<!-- BEGIN LOGO -->
					<div class="page-logo">
						<a href="/panel">
							<img src="/assets/layouts/layout/img/logotip.png" width="140"  alt="logo" class="logo-default" style="margin-top:5px;" /> </a>
						<div class="menu-toggler sidebar-toggler"  style="display: none;">
							<span></span>
						</div>
					</div>
                    <!-- END LOGO -->
                    <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                    <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                        <span></span>
                    </a>
                    <!-- END RESPONSIVE MENU TOGGLER -->
                    <!-- BEGIN TOP NAVIGATION MENU -->
                    <div class="top-menu">
                        <ul class="nav navbar-nav pull-right">
							@if($__env->yieldContent('error'))
							 @yield('error')
							@else
                            @if (Auth::guest())
                        <li><a class="btn grey-mint btn-outline sbold uppercase" href="{{ url('/login') }}" style="height: 100%; border:none;"><i class="fa fa-sign-in" aria-hidden="true" ></i>Login</a></li>
                        <li><a class="btn grey-mint btn-outline sbold uppercase" href="{{ url('/register') }}" style="height: 100%;border:none;"><i class="fa fa-registered" aria-hidden="true"></i>Register</a></li>
						@else
                            <li class="dropdown dropdown-user">
                                <a href="#" class="dropdown-toggle  btn default uppercase btn-block" id="open_dropdown" >
									@if(isset($link))
                                    <img alt="" class="img-circle" src="{!! $link !!}">
									@endif
                                    <span class="username username-hide-on-mobile">{{ Auth::user()->name }}</span>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <ul id="dropdown" class="dropdown-menu dropdown-menu-default">
                                    <li>
                                        <a href="/panel">
                                            <i class="icon-list"></i>Panel </a>
                                    </li>
									<li>
										<a href="/user">
											<i class="icon-user"></i> My Profile </a>
									</li>
                                    <li>
                                        <a href="{{ url('/logout') }}"
										   onclick="event.preventDefault();
												   document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                            <!-- END USER LOGIN DROPDOWN -->
                            <!-- BEGIN QUICK SIDEBAR TOGGLER -->
                            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                            <li class="dropdown dropdown-quick-sidebar-toggler">
                                <a href="javascript:;" class="dropdown-toggle">
                                    <i class="icon-logout"></i>
                                </a>
                            </li>
							@endif
							@endif
                            <!-- END QUICK SIDEBAR TOGGLER -->
                        </ul>
                    </div>
                    <!-- END TOP NAVIGATION MENU -->
                </div>
                <!-- END HEADER INNER -->
            </div>
            <!-- END HEADER -->
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>
            <!-- END HEADER & CONTENT DIVIDER -->
            <!-- BEGIN CONTAINER -->
            <div class="page-container">
                <!-- BEGIN SIDEBAR -->
                <div class="page-sidebar-wrapper">
                    <!-- BEGIN SIDEBAR -->
                    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                    
                    <!-- END SIDEBAR -->
                </div>
                <!-- END SIDEBAR -->
                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content" style="min-height: 910px; margin-left: 0;">
                       @yield('content')
                        
                </div>

            </div>
        </div>
        </div>
<script>
	$(function() {
	$( "#open_dropdown" ).click(function() {
		var visible=$('#dropdown').css("display");
		if(visible==="none"){
			$("#dropdown").show();
			setTimeout(function(){
  $("#dropdown").hide();
}, 5000);
		}else{ $("#dropdown").hide();
		}
	});
	});
</script>
</body></html>