<?php

namespace App;

use Laravel\Socialite\Contracts\User as ProviderUser;
use Hash;
class SocialAccountService
{
	
    public function createOrGetUser(ProviderUser $providerUser)
    {
        $account = SocialAccount::whereProvider('facebook')
            ->whereProviderUserId($providerUser->getId())
            ->first();

        if ($account) {
            return $account->user;
        } else {

            $account = new SocialAccount([
                'provider_user_id' => $providerUser->getId(),
                'provider' => 'facebook'
            ]);
            
            $user = User::whereEmail($providerUser->getEmail())->first();
            if (!$user) {
				$names_users=explode(" ", $providerUser->getName());
                $first_name=$names_users[2];
				$last_name=$names_users[0];
				$middle_name=$names_users[1];
				$fileContents = file_get_contents($providerUser->avatar_original);
				$login=strtolower($first_name);
				$login =strtr($login, array('а'=>'a','б'=>'b','в'=>'v','г'=>'g','д'=>'d','е'=>'e','ё'=>'e','ж'=>'j','з'=>'z','и'=>'i','й'=>'y','к'=>'k','л'=>'l','м'=>'m','н'=>'n','о'=>'o','п'=>'p','р'=>'r','с'=>'s','т'=>'t','у'=>'u','ф'=>'f','х'=>'h','ц'=>'c','ч'=>'ch','ш'=>'sh','щ'=>'shch','ы'=>'y','э'=>'e','ю'=>'yu','я'=>'ya','ъ'=>'','ь'=>'','А'=>'A','Б'=>'B','В'=>'V','Г'=>'G','Д'=>'D','Е'=>'E','Ё'=>'E','Ж'=>'J','З'=>'Z','И'=>'I','Й'=>'Y','К'=>'K','Л'=>'L','М'=>'M','Н'=>'N','О'=>'O','П'=>'P','Р'=>'R','С'=>'S','Т'=>'T','У'=>'U','Ф'=>'F','Х'=>'H','Ц'=>'C','Ч'=>'Ch','Ш'=>'Sh','Щ'=>'Shch','Ы'=>'Y','Э'=>'E','Ю'=>'Yu','Я'=>'Ya','Ъ'=>'','Ь'=>''));
                $user = User::create([
                    'email' => $providerUser->getEmail(),
                    'name' => $login,
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'middle_name' =>$middle_name,
					'password'=>Hash::make($providerUser->getId())
                ]);
				$id = $user->id;
				$user->roles()->attach("2");
				$output_patch = public_path()."/avatars/";
				if (!is_dir($output_patch)) {
					mkdir($output_patch, 0777, true);
				}
				$file = $fileContents;
				$output_patch.= $id . "." . "jpg";
				file_put_contents($output_patch, $file);
            }
            
            $account->user()->associate($user);
            $account->save();

            return $user;

        }

    }
}