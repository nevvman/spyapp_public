<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

use Closure;

class Admins {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle(Request $request, Closure $next) {
		$user = Auth::user();
		if ($user->hasRole('admin')) {
			
		} else {
			abort(403, 'Unauthorized action.');
		}
		return $next($request);
	}

}
