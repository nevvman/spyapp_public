<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\common;
use View;
use Closure;

class Access {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle(Request $request, Closure $next) {
		if (empty($device_id)) {
			$user = Auth::user();
			if ($user->hasRole('admin')) {
				view::share(["Admin"=>"1"]);
				$rows_sql = DB::select('select * from `devices` where 1');
			} else {
				$rows_sql = DB::select('select * from `devices` where `user_id`="' . Auth::id() .'"');
			}
		} else {
			$user = Auth::user();
			if ($user->hasRole('admin')) {
				view::share(["Admin"=>"1"]);
				$rows_sql = DB::select('select * from `devices` where 1');
			} else {
				$rows_sql = DB::select('select * from `devices` where `user_id`="' . Auth::id() . '"AND `id`="' . $device_id . '"');
			}
		}
		$devices = common::object_to_array($rows_sql);
		if (empty($devices)) {
			abort(403, 'Unauthorized action.');
		}
		return $next($request);
	}

}
