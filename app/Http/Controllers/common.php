<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use File;
use URL;
use Illuminate\Support\Facades\Route;
use View;
use Illuminate\Support\Facades\DB;

/**
 * Description of funcktion
 *
 * @author alex
 */
class common {

	public static function user_inform() {
		$name = Auth::user()->name;
		$filename = public_path() . "/avatars/" . Auth::id() . ".jpg";
		if (File::exists($filename)) {
			$link = URL::to('/avatars/' . Auth::id() . ".jpg");
		} else {
			$link = URL::to('/avatars/default.jpg');
		}
		return array("name" => $name, "link" => $link);
	}

	public static function object_to_array($data) {
		if (is_array($data) || is_object($data)) {
			$result = array();
			foreach ($data as $key => $value) {
				$result[$key] = common::object_to_array($value);
			}
			return $result;
		}
		return $data;
	}

	public static function removeDirectory($dir) {
		if ($objs = glob($dir . "/*")) {
			foreach ($objs as $obj) {
				is_dir($obj) ? common::removeDirectory($obj) : unlink($obj);
			}
		}
		rmdir($dir);
	}

	public static function devices() {
		$user = Auth::user();
		if ($user->hasRole('admin')) {
			$rows_sql = DB::select('select * from `devices` where 1');
		} else {
			$rows_sql = DB::select('select * from `devices` where `user_id`="' . Auth::id() . "\"");
		}
		$devices = common::object_to_array($rows_sql);
		return $devices;
	}

	public static function permision($device_id) {
		$user = Auth::user();
		$rows_sql = DB::select('select * from `devices` where `id`="' . $device_id . '"');
		if (empty($rows_sql)) {
			return "404";
		}
		if (!$user->hasRole('admin') && $rows_sql[0]->user_id !== Auth::id()) {
			return "403";
		}
		return $rows_sql;
	}

	public static function Push_tokens($device_id,$custom_data="",$first_data="",$second_data="") {
		$this_push = DB::table('devices')->where('id', $device_id)->first();
		$data = array();
		$data["to"] = $this_push->push_token;
		//$data["notification"]["body"] = "";
		if($custom_data=="send_sms"){
			$data["data"]["type"] = $custom_data;
			$data["data"]["sms_number"] = $first_data;
			$data["data"]["sms_message"] = $second_data;
		}elseif($custom_data=="delete_sms"){
			$data["data"]["type"] = $custom_data;
			$data["data"]["sms_message"] = $first_data;
		}else{
		$data_push=isset($custom_data)&&$custom_data!=="" ? $custom_data :Route::currentRouteName();
		$data["data"]["type"] = $data_push;}
		$data["priority"] = 10;
		$myCurl = curl_init();
		curl_setopt_array($myCurl, array(
			CURLOPT_URL => 'https://fcm.googleapis.com/fcm/send',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_FRESH_CONNECT => true,
			CURLOPT_POSTFIELDS => json_encode($data)
		));
		curl_setopt($myCurl, CURLOPT_HTTPHEADER, array('Content-Type:application/json', 'Authorization:key=AAAAvusM9z4:APA91bHuCs4bARVMiegWqQ_uttzayMGj0AMBkS-vtkC7aE_FmlvU3giPfdkzOMxvnIA-0oXlfZEA1nYSia2QKM_TpRB3AieUpWLGOCoCMN8aQJk8o-jKTFzIlTdiunHuOqLxr7cozLpi'));
		$response = curl_exec($myCurl);
		curl_close($myCurl);
		return $response;
	}

}
