<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers\User;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use Hash;
use App\Http\Controllers\Controller;
use App\Http\Controllers\common;

/**
 * Description of User
 *
 * @author alex
 */
class User extends Controller {

	public function Account() {
		$user = common::user_inform();
		return View('user', ["link" => $user["link"]]);
	}

	public function Edit(Request $request) {
		$users_data = $request->request->all();
		if (isset($users_data["old_password"])and isset($users_data["new_password"])) {
			if (Hash::check($users_data["old_password"], Auth::user()->password)) {
				$user = Auth::user();
				$user->password = Hash::make($users_data["new_password"]);
				$user->save();
				session()->flash('msg', 'Account updated');
				return redirect('/user');
			} else {
				session()->flash('msg', "Password incorrect" . Auth::user()->password);
				return redirect('/user');
			}
		}
		$user = Auth::user();
		$img = $request->files->get('img');
		if (isset($img)) {
			$output_patch = public_path() . "/avatars/";
			if (!is_dir($output_patch)) {
				mkdir($output_patch, 0777, true);
			}
			$img->move(public_path() . "/avatars/", Auth::id() . '.' . "jpg");
			session()->flash('msg', 'Avatar updated');
			return redirect('/user');
		} else {
			if ($users_data["first_name"] !== $user->first_name) {
				$user->first_name = $users_data["first_name"];
			} else {
				
			}
			if ($users_data["last_name"] !== $user->last_name) {
				$user->last_name = $users_data["last_name"];
			} else {
				
			}
			if ($users_data["middle_name"] !== $user->middle_name) {
				$user->middle_name = $users_data["middle_name"];
			} else {
				
			}
			if ($users_data["email"] !== $user->email) {
				$user->email = $users_data["email"];
			}
			if ($users_data["name"] !== $user->name) {
				$user->name = $users_data["name"];
			}
		}
		$user->save();
		session()->flash('msg', 'Account updated');
		return redirect('/user');
	}

}
