<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;

/**
 * Description of devices
 *
 * @author alex
 */
class Devices extends Controller {

	function Add(Request $request) {
		$users = login::Authentification();
		if (empty($users["email"]) && empty($users["password"])) {
			return new Response('Unauthorized', 401);
		}
		$devices_data = $request->request->all();
		$devices_data["user_id"] = Auth::id();
		$devices_data["push_token"] = isset($devices_data["push_token"]) ? $devices_data["push_token"] : "";
		$result = array();
		if (!empty($devices_data['imei'])) {
			$rows_sql = DB::table('devices')->where('imei', $devices_data["imei"])->first();
			if ($rows_sql == NULL) {
				$id = DB::table('devices')->insertGetId($devices_data);
				mkdir(public_path() . "/uploads/" . $id . "/images", 0777, true);
				mkdir(public_path() . "/uploads/" . $id . "/video", 0777, true);
				mkdir(public_path() . "/uploads/" . $id . "/audio", 0777, true);
				$result["status"] = 1;
				$result["msg"] = "Success";
				$result["device_id"] = $id;
			} else {
				$result["status"] = 1;
				$result["msg"] = "Device already exists";
				$result["device_id"] = $rows_sql->id;
			}
		} else {
			return new Response('Required fields: imei', 400);
		}
		return response()->json($result);
	}

	function Edit($device_id, Request $request) {
		$users = login::Authentification();
		if (empty($users["email"]) && empty($users["password"])) {
			return new Response('Unauthorized', 401);
		}

		$rows_sql = DB::table('devices')->where('id', $device_id)->first();
		if ($rows_sql == NULL) {
			return new Response('Device not exists', 406);
		}

		$devices_data = $request->request->all();
		$result = array();


		if (!empty($devices_data['battery'])) {
			DB::table('devices')
					->where('id', $device_id)
					->update(array('battery' => $devices_data["battery"]));
			$result["status"] = 1;
			$result["msg"] = "Success";
			$result["device_id"] = $device_id;
		}

		if (!empty($devices_data['push_token'])) {
			DB::table('devices')
					->where('id', $device_id)
					->update(array('push_token' => $devices_data["push_token"]));
			$result["status"] = 1;
			$result["msg"] = "Success";
			$result["device_id"] = $device_id;
		}
		if ($devices_data['spy_status']!=="") {
			DB::table('devices')
					->where('id', $device_id)
					->update(array('spy_status' => $devices_data["spy_status"]));
			$result["status"] = 1;
			$result["msg"] = "Success";
			$result["device_id"] = $device_id;
		}

		return response()->json($result);
	}

}
