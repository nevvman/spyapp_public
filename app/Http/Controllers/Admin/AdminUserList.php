<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use App\Http\Controllers\Controller;
use App\Http\Controllers\common;
use App\Http\Controllers\queryData;

class AdminUserList extends Controller {

	public function users() {
		$user = common::user_inform();
		$devices = common::devices();
		foreach ($devices as $key => $value) {
			$str = stripos($value["os"], "Android");
			if ($str === 0) {
				$devices[$key]["logo"] = "android.png";
			} else {
				$str2 = stripos($value["os"], "Apple");
				if ($str2 === 0) {
					$devices[$key]["logo"] = "apple.png";
				} else {
					$str3 = stripos($value["os"], "Windows");
					if ($str3 === 0) {
						$devices[$key]["logo"] = "windows.png";
					} else {
						$devices[$key]["logo"] = "android.png";
					}
				}
			}
		}
		$users_sql = array(
			"id",
			'first_name',
			'last_name',
			'middle_name',
			'name',
			'email',
			'created_at'
		);
		$users_header = array(
			"Devices",
			'First name',
			'Last name',
			'Middle name',
			'Name',
			'Email',
			'Date register'
		);
		return View::make('adminpanel.list_user', [ "table_message" => $users_sql, "headers" => $users_header,"device_id"=>"1", "table_device" => $devices, "names" => $user["name"], "link" => $user["link"], "route" => "admin"]);
	}

	public function list_table(Request $request) {
		$start = 0;
		$vars = $request->query->all();
		$qsStart = (int) $vars["start"];
		$search = $vars["search"];
		$order = $vars["order"];
		$columns = $vars["columns"];
		$qsLength = (int) $vars["length"];
		$search_array = [];
		$data_array = [];
		for ($args = 0; $args <= 5; $args++) {
			if ($columns[$args]["search"]["value"] != NULL) {
				$search_array[$columns[$args]["data"]] = $columns[$args]["search"]["value"];
				$data_array = $columns[$args]["data"];
			}
		}

		if ($qsStart) {
			$start = $qsStart;
		}

		$index = $start;
		$rowsPerPage = $qsLength;

		$rows = array();

		$searchValue = $search['value'];
		$orderValue = $order[0];

		$orderClause = "";
		if ($orderValue) {
			$orderClause = " ORDER BY " . $columns[(int) $orderValue['column']]['data'] . " " . $orderValue['dir'];
		}
		$table_columns = array(
			"id",
			'first_name',
			'last_name',
			'middle_name',
			'name',
			'email',
			'created_at'
		);

		$whereClause = "";
		$i = 0;
		if (!empty($search_array)) {
			foreach ($search_array as $key => $value) {

				if ($i > 0) {
					$whereClause = $whereClause . " AND";
				}

				$whereClause = $whereClause . " " . $key . " LIKE '%" . $value . "%'";


				$i = $i + 1;
			}
		}
		$records = DB::select("SELECT COUNT(*) FROM `users`" . $whereClause . $orderClause);
		$records = common::object_to_array($records);
		$recordsTotal = $records[0]["COUNT(*)"];
		$find_sql = DB::select("SELECT * FROM `users`" . $whereClause . $orderClause . " LIMIT " . $index . "," . $rowsPerPage);
		$rows_sql = common::object_to_array($find_sql);
		foreach ($rows_sql as $row_key => $row_sql) {
			for ($i = 0; $i < count($table_columns); $i++) {
				if ($table_columns[$i] == "id") {
					$device_sql = DB::select('select * from `devices` where `user_id`="' . $row_sql[$table_columns[$i]] . "\"");
					$devices = common::object_to_array($device_sql);
					foreach ($devices as $key => $value) {
						$str = stripos($value["os"], "Android");
						if ($str === 0) {
							$devices[$key]["logo"] = "android.png";
						} else {
							$str2 = stripos($value["os"], "Apple");
							if ($str2 === 0) {
								$devices[$key]["logo"] = "apple.png";
							} else {
								$str3 = stripos($value["os"], "Windows");
								if ($str3 === 0) {
									$devices[$key]["logo"] = "windows.png";
								} else {
									$devices[$key]["logo"] = "android.png";
								}
							}
						}
					}
					$device_list = '<div class="mt-list-container list-news"><ul>';
					foreach ($devices as $key => $value) {
						$device_list = $device_list . '<li class="mt-list-item"><div class="list-item-content"><center><a href="/panel/device/' . $value["id"] . '"><img alt="' . $value["logo"] . '" width="45" height="auto" class="img" src="/images/os/' . $value["logo"] . '"><span>' . $value["name"] . '</span></a></center></div></li>';
					}
					$device_list = $device_list . "</ul></div>";
					$content = '<button type="button" class="btn green  " data-toggle="modal" data-target="#myModal_' . $row_sql[$table_columns[$i]] . '">View device list</button>'
							. '<div class="modal fade" id="myModal_' . $row_sql[$table_columns[$i]] . '" role="dialog">'
							. '<div class="modal-dialog"><div class="modal-content"><div class="modal-header">'
							. '<button type="button" class="close" data-dismiss="modal">&times;</button>'
							. '<h4 class="modal-title">Devices of user</h4>'
							. '</div><div class="modal-body"><div class="mt-element-list"><div class="mt-list-head list-news font-white bg-blue"> <div class="list-head-title-container"><h3 class="list-title">Device List</h3></div></div>'
							. $device_list
							. '</div></div></div></div>';
					$rows[$row_key][$table_columns[$i]] = $content;
				} else {
					$rows[$row_key][$table_columns[$i]] = $row_sql[$table_columns[$i]];
				}
			}
		}

		$queryData = new queryData();
		$queryData->start = $start;
		$queryData->recordsTotal = $recordsTotal;
		$queryData->recordsFiltered = $recordsTotal;
		$queryData->data = $rows;
		return response()->json($queryData);
	}

}
