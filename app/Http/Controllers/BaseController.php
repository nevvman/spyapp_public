<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers;

use View;

/**
 * Description of BaseController
 *
 * @author alex
 */
class BaseController extends Controller {

	public function __construct() {

		$typ = [
			"whatsapp"	=> "WhatsApp",
			"fb"		=> "Facebook",
			"viber"		=> "Viber",
			"imessage"	=> "iMessage",
			"bbm"		=> "BBM",
			"yahoo"		=> "Yahoo Messenger",
			"snapchat"	=> "Snapchat",
			"hangouts"	=> "Hangouts",
			"kik"		=> "KIK",
			"telegram"	=> "Telegram",
			"tinger"	=> "Tinder",
			"SMS"		=> "SMS",
			"MMS"		=> "MMS",
			"line"		=> "LINE",
			"skype"		=> "Skype",
			"instagram" => "Instagram",
			"wechat"	=> "WeChat",
			"email"		=> "Email",
			];
		View::share(["typ" => $typ]);
	}

}
